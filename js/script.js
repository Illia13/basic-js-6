"use strict";

//! 1. Створіть об'єкт product з властивостями name, price та discount.
//! Додайте метод для виведення повної ціни товару з урахуванням знижки. Викличте цей метод та результат виведіть в консоль.

// const product = {
//   name: "Mobile",
//   price: 5000,
//   discount: 20,
// };

// product.discountProduct = function () {
//   let result = this.price - this.price * (this.discount / 100);
//   return result;
// };

// console.log(product.discountProduct());

//! 2. Напишіть функцію greeting, яка приймає об'єкт з властивостями name та age, і повертає рядок з привітанням і віком,
//! наприклад "Привіт, мені 30 років".
//! Попросіть користувача ввести своє ім'я та вік
//! за допомогою prompt, і викличте функцію gteeting з введеними даними(передавши їх як аргументи).
//!  Результат виклику функції виведіть з допомогою alert.

// function userNameAge() {
//   let name = prompt("Введіть своє ім'я:");
//   let age = prompt("Введіть свій вік:");
//   const user = {
//     name: name,
//     age: age,
//   };

//   return `Привіт мене звуть ${name} та мені ${age} років!`;
// }

// alert(userNameAge());

//! 3.Опціональне. Завдання:
//! Реалізувати повне клонування об'єкта.

const product = {
  name: "Mobile",
  price: 5000,
  model: 10,
};

function cloneFn(obj) {
  if (typeof obj !== "object") {
    return obj;
  }
  const clone = Array.isArray(obj) ? [] : {};
  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      if (typeof obj[key] === "object" && obj[key] !== null) {
        clone[key] = cloneFn(obj[key]);
      } else {
        clone[key] = obj[key];
      }
    }
  }
  return clone;
}
const cloneProduct = cloneFn(product);
console.log(cloneProduct);
